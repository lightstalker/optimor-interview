import unittest
from nose_parameterized import parameterized
from collections import OrderedDict
from ExtractO2 import extract_multiple_standard_rates, extract_standard_rate


class TestExtractO2(unittest.TestCase):
    def setUp(self):
        self.url = "http://international.o2.co.uk/internationaltariffs/calling_abroad_from_uk"

    @parameterized.expand([
        ["Canada", "£1.50"],
        ["Germany", "£1.50"],
        ["Iceland", "£1.50"],
        ["Pakistan", "£2.00"],
        ["Singapore", "£1.50"],
        ["South Africa", "£1.50"]
    ])
    def test_country(self, country, expected):
        actual = extract_standard_rate(self.url, country)
        self.assertEquals(actual, expected)

    def test_all_countries(self):
        countries = ["Canada", "Germany", "Iceland", "Pakistan", "Singapore", "South Africa"]
        expected_output = OrderedDict(
            [("Canada", "£1.50"), ("Germany", "£1.50"), ("Iceland", "£1.50"), ("Pakistan", "£2.00"),
             ("Singapore", "£1.50"), ("South Africa", "£1.50")])
        actual = extract_multiple_standard_rates(self.url, countries)
        self.assertEquals(actual, expected_output)