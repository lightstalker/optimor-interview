Timeout set to 10 miliseconds
Browser set to chrome

Issues present:
	timeouts and interaction by person running script can cause issues, assuming these are common to selenium


Recommend to move away from selenium when possible, renders the entire site when entire site not required, using utilities like BeautifulSoup instead

Tested on Windows using python 3.4.2