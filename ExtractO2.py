from collections import OrderedDict
import selenium.common.exceptions

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

#Assumption computer has chrome turn into variable if needed in future
driver = webdriver.Chrome()
wait = WebDriverWait(driver, 10)

def extract_standard_rate(url, country, contract_type="paymonthly", calling="Landline"):
    driver.get(url)  # TODOFUTURE efficiency, put into class
    wait.until(EC.presence_of_element_located((By.ID, "countryName")))
    # input country name into form
    elem = driver.find_element_by_id("countryName")
    elem.clear()
    elem.send_keys(country)
    elem.send_keys(Keys.RETURN)
    # select tab on page
    wait.until(EC.presence_of_element_located((By.ID, contract_type)))
    elem = driver.find_element_by_id(contract_type)
    elem.send_keys(Keys.RETURN)
    #extract table and specific element from table
    wait.until(EC.presence_of_element_located((By.ID, "standardRatesTable")))
    table = driver.find_element_by_id("standardRatesTable")

    wait.until(EC.visibility_of(table))
    for row in table.find_elements_by_xpath('.//tr'):
        row_detail = row.text.split()
        if calling == row_detail[0]:
            return row_detail[1]
    raise selenium.common.exceptions.NoSuchElementException(calling)  # not sure if most suitable


def extract_multiple_standard_rates(url, countries, contract_type="paymonthly", calling="Landline"):
    return OrderedDict((country, extract_standard_rate(url, country, contract_type, calling)) for country in countries)


if __name__ == '__main__':
    countries = ["Canada", "Germany", "Iceland", "Pakistan", "Singapore", "South Africa"]
    url = "http://international.o2.co.uk/internationaltariffs/calling_abroad_from_uk"
    rates = extract_multiple_standard_rates(url, countries)
    driver.close()
    for country, rate in rates.items():
        print(country, rate)